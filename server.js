var express = require('express');
var request = require('request');

var app = express();
app.use('/interactive-maps', express.static(__dirname + '/src/main/webapp'));
app.use('/interactive-maps/api', express.static(__dirname + '/src/main/webapp'));
app.use('/ogc-client/rest/json', function (req, res) {
    req.pipe(request(req.query.endpoint)).pipe(res);
});

app.listen(process.env.PORT || 3000);