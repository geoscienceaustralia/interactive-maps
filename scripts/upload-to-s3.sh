#!/bin/bash -ex

WEB_BUCKET=${DEPLOY_ENV}_BUCKET

aws configure set aws_access_key_id ${AWS_ACCESS_KEY} --profile default
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY} --profile default
aws s3 sync --delete ${ARTEFACT} s3://${!WEB_BUCKET} --exact-timestamps
