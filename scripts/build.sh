#!/bin/bash -ex

npm install -g bower@1.8.12
npm install -g grunt@1.3.0
npm install --unsafe-perm
bower install --allow-root
grunt build
cd build
npm install
zip -r /tmp/artifact.zip *